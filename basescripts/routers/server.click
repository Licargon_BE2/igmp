 
///===========================================================================///
/// An IP router with 1 interface.
elementclass Server 
{
$addr_info, $gateway
|


/// ===== Begin - IP Routing Path - Network Layer
	// Shared IP input path and routing table
	ip :: Strip(14)
	-> CheckIPHeader()
	-> rt :: StaticIPLookup(
		$addr_info:ip/32 0,
		$addr_info:ipnet 0,
		0.0.0.0/0.0.0.0 $gateway 1);
/// ===== End - IP Routing Path - Network Layer

/// ===== Begin - ARP Path - MAC to Network Layer
	// ARP responses are copied to each ARPQuerier and the host.
	arpt :: Tee(1);
	
	// Input and output paths for eth0
	// Filter out packets with dest 224.0.0.1
	igmpFilter::IPClassifier(dst host 224.0.0.1, -);
	c0 :: Classifier(12/0806 20/0001, 12/0806 20/0002, -);
	input[0] -> HostEtherFilter($addr_info:eth) -> c0;
	c0[0] -> ar0 :: ARPResponder($addr_info) -> [0]output;
	arpq0 :: ARPQuerier($addr_info, $addr_info) -> [0]output;
	c0[1] -> arpt;
	arpt[0] -> [1]arpq0;
	c0[2] -> [0]igmpFilter[0] -> ToDump("Filtered.dump") -> Discard;
	// All packets that dont match "dst host 224.0.0.1": treat like before
	igmpFilter[1] -> Paint(1) -> ip; 
/// ===== End - ARP Path - MAC to Network Layer

/// ===== Begin - Local Delivery - Transport and Application Layer	
/// ===== Note: any packets meant for this node should be processed here, or passed to the output
	// Local delivery
	rt[0] -> [1]output; 
/// ===== End - Local Delivery - Transport and Application Layer

/// ===== Begin - Forwarding Path - Network Layer	
	rt[1] -> DropBroadcasts
	-> gio0 :: IPGWOptions($addr_info)
	-> FixIPSrc($addr_info)
	-> dt0 :: DecIPTTL
	-> fr0 :: IPFragmenter(1500)
	-> [0]arpq0;
	dt0[1] -> ICMPError($addr_info, timeexceeded) -> rt;
	fr0[1] -> ICMPError($addr_info, unreachable, needfrag) -> rt;
	gio0[1] -> ICMPError($addr_info, parameterproblem) -> rt;
/// ===== End - Forwarding Path - Network Layer	
}
