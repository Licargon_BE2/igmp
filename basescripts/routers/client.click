///===========================================================================///
/// An IP router with 1 interface.
elementclass Client 
{
$addr_info, $gateway
|

AddressInfo(destAddr 224.0.0.22 01:00:5e:00:00:01)
clientCPU::ClientProcessor()


/// ===== Begin - IP Routing Path - Network Layer
	// Shared IP input path and routing table
	ip :: Strip(14)
	-> CheckIPHeader()
	-> rt :: StaticIPLookup(
		224.0.0.0/4 2,
		$addr_info:ip/32 0,
		$addr_info:ipnet 0,
		0.0.0.0/0.0.0.0 $gateway 1); // Multicasted packets come out of output 2
/// ===== End - IP Routing Path - Network Layer
	rt[2] -> Discard;
/// ===== Begin - ARP Path - MAC to Network Layer
	// ARP responses are copied to each ARPQuerier and the host.
	arpt :: Tee(1);
	
	// Classifier to pre-emptively filter out UDP packets
	udpClass::IPClassifier("udp", -); 
	// Filter out queries
	queryClass::IPClassifier("ip proto 2", -);
	
	// Input and output paths for eth0
	c0 :: Classifier(12/0806 20/0001, 12/0806 20/0002, -);
	
	// Route the input through the classifier to get the pings out, otherwise it f**ks up
	input[0] -> udpClass[0] -> ToDump("ClientIncomingUDP.dump") -> [1]clientCPU;
	// Route the rest as normal
	udpClass[1] -> [0]queryClass;
	queryClass[0] -> ToDump("ClientIncomingQueries.dump") -> [2]clientCPU[4] -> MarkIPHeader -> IPEncap(2, $addr_info, destAddr, TTL 1) -> EtherEncap(0x0800, $addr_info, destAddr) -> ToDump("ClientOutgoingResponses.dump") -> [0]output; 
	queryClass[1] -> HostEtherFilter($addr_info:eth) -> c0;
	
	c0[0] -> ar0 :: ARPResponder($addr_info) -> [0]output;
	arpq0 :: ARPQuerier($addr_info, $addr_info) -> [0]output;
	c0[1] -> arpt;
	arpt[0] -> [1]arpq0;
	c0[2] -> Paint(1) -> ip;
/// ===== End - ARP Path - MAC to Network Layer

/// ===== Begin - Local Delivery - Transport and Application Layer	
/// ===== Note: any packets meant for this node should be processed here, or passed to the output
	// Local delivery
	rt[0] -> Discard; 
	
	// Element listening for handlers calls to send out join or leave reports
	clientCPU[0] 
	    -> MarkIPHeader
	    -> IPEncap(2, $addr_info, destAddr, TTL 1) 
	    -> EtherEncap(0x0800, $addr_info, destAddr)
	    -> ToDump("ClientHandlerOutgoing.dump")
	    -> [0]clientCPU;
	    
	// Output 0: Packets going back to the network, forwarded reports
	clientCPU[3] -> [0]output;
	// Output 1: Accepted UDP pings, locally delivered
	clientCPU[1] -> [1]output;
	// Output 2: Non-accepted UDP pings: Discard
	clientCPU[2] -> Discard;
	
	
/// ===== End - Local Delivery - Transport and Application Layer

/// ===== Begin - Forwarding Path - Network Layer	
	rt[1] -> DropBroadcasts
	-> gio0 :: IPGWOptions($addr_info)
	-> FixIPSrc($addr_info)
	-> dt0 :: DecIPTTL
	-> fr0 :: IPFragmenter(1500)
	-> [0]arpq0;
	dt0[1] -> ICMPError($addr_info, timeexceeded) -> rt;
	fr0[1] -> ICMPError($addr_info, unreachable, needfrag) -> rt;
	gio0[1] -> ICMPError($addr_info, parameterproblem) -> rt;
/// ===== End - Forwarding Path - Network Layer	
}
