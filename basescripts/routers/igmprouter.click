///===========================================================================///
/// An IP router with 3 interfaces.
elementclass IGMPRouter
{
$server_address, $client_address1, $client_address2
|

AddressInfo(genQueryDestAddr 224.0.0.1 01:00:5e:00:00:01)
routerInfoBase::RouterInfoBase(); 

/// ===== Begin - IP Routing Path - Network Layer
	// Shared IP input path and routing table
	ip :: Strip(14)
	-> CheckIPHeader()
	-> rt :: StaticIPLookup(
		$server_address:ip/32 0,
		$client_address1:ip/32 0,
		$client_address2:ip/32 0,
		$server_address:ipnet 1,
		$client_address1:ipnet 2,
		$client_address2:ipnet 3,
		224.0.0.0/4 4); // Hardcoded. Needs to be changed.
		// The idea is here to intercept msgs to our multicast group, and relay them through our own elements to find out
		// which clients want it

/// ===== End - IP Routing Path - Network Layer

	queryTee::Tee(2) // Copy the query for both client networks
	

	routerInfoBase[3]
		-> GenerateIGMPMembershipQuery() 
	    -> MarkIPHeader
	    -> IPEncap(2, $server_address, genQueryDestAddr, TTL 1) 
	    -> EtherEncap(0x0800, $server_address, genQueryDestAddr)
	    -> queryTee

	queryTee[0] -> [1]output
	queryTee[1] -> [2]output
/// ===== Begin - ARP Path - MAC to Network Layer	
/// ===== Note: incoming Ethernet encapsulated packets, per interface
	// ARP responses are copied to each ARPQuerier and the host.
	arpt :: Tee (3);
	
	// Input and output paths for eth0
	// eth0 goes to the servernetwork
	igmpClassifier0::IPClassifier(ip proto 2, -);
	generalQueryClassifier::IPClassifier(dst host 224.0.0.1, -); // Filter out all the general queries
	c0 :: Classifier(12/0806 20/0001, 
			 12/0806 20/0002,
			 -);
	input[0] -> HostEtherFilter($server_address:eth) -> c0;
	c0[0] -> ar0 :: ARPResponder($server_address) -> [0]output;
	arpq0 :: ARPQuerier($server_address, $server_address) -> [0]output;
	c0[1] -> arpt;
	arpt[0] -> [1]arpq0;
	// Reroute the non-ARP through our IGMPfilter, non-matches go through the ip element
	c0[2] -> [0]igmpClassifier0;
	igmpClassifier0[0] -> generalQueryClassifier[0] -> Discard;
	generalQueryClassifier[1] -> Discard;
	igmpClassifier0[1] -> Paint(1) -> ip;

	routerInfoBase[0] -> [1]output;
	routerInfoBase[1] -> [2]output;
	routerInfoBase[2] -> SetIPChecksum -> EtherEncap(0x0800, $server_address, $client_address1) -> c0;
	
	rt[4] -> EtherEncap(0x0800, $server_address, $client_address1) -> [2]routerInfoBase;
	
	// Input and output paths for eth1
	// Interface to Client network 1
	igmpClassifier1::IPClassifier(ip proto 2 dst host 224.0.0.22, -); // Filter out IGMP reports from client network 1
	c1 :: Classifier(12/0806 20/0001,
			 12/0806 20/0002,
			 -);
	input[1] -> HostEtherFilter($client_address1:eth) -> c1;
	c1[0] -> ar1 :: ARPResponder($client_address1) -> [1]output;
	arpq1 :: ARPQuerier($client_address1, $client_address1) -> [1]output;
	c1[1] -> arpt;
	arpt[1] -> [1]arpq1;
	c1[2] -> [0]igmpClassifier1;
	igmpClassifier1[0] -> [0]routerInfoBase;
	igmpClassifier1[1] -> Paint(2) -> ip;


	// Input and output paths for eth2
	// Interface to client network 2
	igmpClassifier2::IPClassifier(ip proto 2 dst host 224.0.0.22, -); // Filter out IGMP reports from client network 2
	c2 :: Classifier(12/0806 20/0001,
			 12/0806 20/0002, 
			 -);
	input[2] -> HostEtherFilter($client_address2:eth) -> c2;
	c2[0] -> ar2 :: ARPResponder($client_address2) -> [2]output;
	arpq2 :: ARPQuerier($client_address2, $client_address2) -> [2]output;
	c2[1] -> arpt;
	arpt[2] -> [1]arpq2;
	c2[2] -> [0]igmpClassifier2;
	igmpClassifier2[0] -> [1]routerInfoBase;
	igmpClassifier2[1] -> Paint(3) -> ip;

/// ===== End - ARP Path - MAC to Network Layer

/// ===== Begin - Local Delivery - Transport and Application Layer	
/// ===== Note: any packets meant for this node should be processed here, or passed to the output	
	// Local delivery
	rt[0] -> [3]output; 
/// ===== End - Local Delivery - Transport and Application Layer

/// ===== Begin - Forwarding Path - Network Layer
/// ===== Note: outgoing IP packets, per interface	
	// Forwarding path for eth0
	rt[1] -> DropBroadcasts 
	-> cp0 :: PaintTee(1)
	-> gio0 :: IPGWOptions($server_address)
	-> FixIPSrc($server_address)
	-> dt0 :: DecIPTTL
	-> fr0 :: IPFragmenter(1500)
	-> [0]arpq0;
	dt0[1] -> ICMPError($server_address, timeexceeded) -> rt;
	fr0[1] -> ICMPError($server_address, unreachable, needfrag) -> rt;
	gio0[1] -> ICMPError($server_address, parameterproblem) -> rt;
	cp0[1] -> ICMPError($server_address, redirect, host) -> rt;
	
	// Forwarding path for eth1
	rt[2] -> DropBroadcasts
	-> cp1 :: PaintTee(2)
	-> gio1 :: IPGWOptions($client_address1)
	-> FixIPSrc($client_address1)
	-> dt1 :: DecIPTTL
	-> fr1 :: IPFragmenter(1500)
	-> [0]arpq1;
	dt1[1] -> ICMPError($client_address1, timeexceeded) -> rt;
	fr1[1] -> ICMPError($client_address1, unreachable, needfrag) -> rt;
	gio1[1] -> ICMPError($client_address1, parameterproblem) -> rt;
	cp1[1] -> ICMPError($client_address1, redirect, host) -> rt;

	// Forwarding path for eth2
	rt[3] -> DropBroadcasts
	-> cp2 :: PaintTee(3)
	-> gio2 :: IPGWOptions($client_address2)
	-> FixIPSrc($client_address2)
	-> dt2 :: DecIPTTL
	-> fr2 :: IPFragmenter(1500)
	-> [0]arpq2;
	dt2[1] -> ICMPError($client_address2, timeexceeded) -> rt;
	fr2[1] -> ICMPError($client_address2, unreachable, needfrag) -> rt;
	gio2[1] -> ICMPError($client_address2, parameterproblem) -> rt;
	cp2[1] -> ICMPError($client_address2, redirect, host) -> rt;
/// ===== End - Forwarding Path - Network Layer	
}

