///================================================================///
/// igmp.click
///
/// This script implements a small IP network inside click. Several
/// compound elements are used to create the different network entities.
///
/// This script is part of the assignment of the 
/// course 'Telecommunicatiesystemen 2006-2007'. The assignment explains
/// in more detail the network architecture.
///
/// Authors: Bart Braem & Michael Voorhaen
///================================================================///

require(library routers/server.click);
require(library routers/client.click);
require(library routers/igmprouter.click);

///===========================================================================///
/// Definitions of the different hosts and related address information.

// @TODO the idea is that instead of sending the data to a unicast address, it should be sent to 
// a multicast address and that all  clients subscribed to this multicast group receive the UDP 
// packet and print this out.
AddressInfo(multicast_client_address 224.0.0.15) 

/// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/// !!!!!!! DO NOT EDIT BELOW THIS LINE: Any changes made below, will be replaced prior to the project defense !!!!!!!!
/// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

// Note: The ListenEtherSwitch is used to emulate a local IP network.

server_network :: ListenEtherSwitch;
AddressInfo(router_server_network_address 192.168.1.254/24 00:50:BA:85:84:A1);
AddressInfo(mulicast_server_address 192.168.1.1/24 00:50:BA:85:84:A2);
multicast_server :: Server(mulicast_server_address, router_server_network_address);

client_network1 :: ListenEtherSwitch;
AddressInfo(router_client_network1_address 192.168.2.254/24 00:50:BA:85:84:B1);
AddressInfo(client21_address 192.168.2.1/24 00:50:BA:85:84:B2);
AddressInfo(client22_address 192.168.2.2/24 00:50:BA:85:84:B3);
client21 :: Client(client21_address, router_client_network1_address);
client22 :: Client(client22_address, router_client_network1_address);

client_network2 :: ListenEtherSwitch;
AddressInfo(router_client_network2_address 192.168.3.254/24 00:50:BA:85:84:C1);
AddressInfo(client31_address 192.168.3.1/24 00:50:BA:85:84:C2);
AddressInfo(client32_address 192.168.3.2/24 00:50:BA:85:84:C3);
client31 :: Client(client31_address, router_client_network2_address);
client32 :: Client(client32_address, router_client_network2_address);

router :: IGMPRouter(router_server_network_address, router_client_network1_address, router_client_network2_address);

///===========================================================================///
/// The configuration of our small IP network

multicast_server[0]
	-> [0]server_network[0]
	-> [0]multicast_server

router[0]
	-> [1]server_network[1]
	-> [0]router
	
client21[0]
	-> [0]client_network1[0]
	-> [0]client21

client22[0]
	-> [1]client_network1[1]
	-> [0]client22

router[1]
	-> [2]client_network1[2]
	-> [1]router

client31[0]
	-> [0]client_network2[0]
	-> [0]client31

client32[0]
	-> [1]client_network2[1]
	-> [0]client32

router[2]
	-> [2]client_network2[2]
	-> [2]router

server_network[2]
	-> ToDump("server_network.dump");

client_network1[3]
	-> ToDump("client_network1.dump");

client_network2[3]
	-> ToDump("client_network2.dump");

/// let the corresponding node send a ping
//ICMPPingSource(mulicast_server_address, client22_address)
RatedSource("data", 1, -1, true)
	-> DynamicUDPIPEncap(mulicast_server_address:ip, 1234, multicast_client_address:ip, 1234) 
	-> EtherEncap(0x0800, mulicast_server_address:eth, mulicast_server_address:eth) /// The MAC addresses here shoudl be from the multicast_server to get past the HostEtherFilter. This way we can reuse the input from the network for the applications.
	-> IPPrint("multicast_server -- UDP ping")
	-> [0]multicast_server

multicast_server[1]
	-> Discard; 

/// packets destined for the mobile node
client21[1]
	-> IPPrint("client21 -- got UDP ping") 
	-> Discard

/// packets destined for the mobile node
client22[1]
	-> IPPrint("client22 -- got UDP ping") 
	-> Discard


/// packets destined for the mobile node
client31[1]
	-> IPPrint("client31 -- got UDP ping") 
	-> Discard

/// packets destined for the mobile node
client32[1]
	-> IPPrint("client32 -- got UDP ping") 
	-> Discard


/// packets destined for the mobile node
router[3]
	-> IPPrint("router -- got UDP ping") 
	-> Discard