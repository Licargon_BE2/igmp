#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include "IGMPJoin.hh"


CLICK_DECLS

IGMPJoin::IGMPJoin() :_timer(this)
{ 
}

IGMPJoin::~IGMPJoin(){}

int IGMPJoin::configure(Vector<String>& conf, ErrorHandler* errh){
	if (cp_va_kparse(conf, this, errh,cpEnd) < 0) return -1;
}

int IGMPJoin::initialize(ErrorHandler *errh){
  _timer.initialize(this);
  _timer.schedule_after_msec(1000);
  return 0;
}

void  IGMPJoin::run_timer(Timer *){
  Packet *q = make_packet("1.2.3.4");
  output(0).push(q);
  _timer.schedule_after_msec(1000);
}

void IGMPJoin::push(int, Packet *p){
	output(0).push(p);
}

Packet* IGMPJoin::make_packet(String grpIP){
    int tailroom = 0;
    int packetsize = sizeof(igmp_membership_query_header);
    int headroom = sizeof(click_ip) + sizeof(click_udp) + sizeof(click_ether);
    WritablePacket* packet = Packet::make(headroom, 0, packetsize, tailroom);
    if (packet == 0) { 
      click_chatter("Cannot make packet");
      return 0;
    }
    memset(packet->data(), 0, packet->length());
    
    igmp_membership_query_header* igmp = (igmp_membership_query_header*) packet->data();
    igmp->type = 0x11;
    igmp->respCode = 105; // Calculated in tenths of a second. 100 = 10.0s
    igmp->groupAddress = IPAddress(grpIP).addr();
    igmp->resANDsqrv = 0;
    igmp->QQIC = 10;
    
    click_chatter("Packet made");
    
    return packet;    
}


int IGMPJoin::joinHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh) {
	click_chatter("JOINED");
//    IGMPJoin* element = (IGMPJoin*) e;
//    String grpIP;
//    if (cp_va_kparse(conf, element, errh, "ip", cpkM, cpString, &grpIP, cpEnd) < -1)
//	click_chatter("Something went wrong");
//
//    element->execute(grpIP);

}

int IGMPJoin::leaveHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh) {
	click_chatter("LEFT");
	//IGMPJoin* element = (IGMPJoin*) e;
	//String grpIP;
	//if (cp_va_kparse(conf, element, errh, "ip", cpkM, cpString, &grpIP, cpEnd) < -1)
	//click_chatter("Something went wrong");
//
	//element->execute(grpIP);
}


void IGMPJoin::execute(String grpIP) {
	Packet *q = make_packet(grpIP);
  	output(0).push(q);
}

void IGMPJoin::add_handlers(){
    add_write_handler("join_group", &joinHandler, (void *)0);
    add_write_handler("leave_group", &leaveHandler, (void *)0);
}




CLICK_ENDDECLS
EXPORT_ELEMENT(IGMPJoin)
