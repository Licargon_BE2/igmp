#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include "MakeIGMPReport.hh"
#include "IGMPGroupRecordHeader.hh"
CLICK_DECLS

MakeIGMPReport::MakeIGMPReport(){}
MakeIGMPReport::~MakeIGMPReport(){}

int MakeIGMPReport::configure(Vector<String>& conf, ErrorHandler* errh){
	if (cp_va_kparse(conf, this, errh,cpEnd) < 0) return -1;
}

int MakeIGMPReport::initialize(ErrorHandler *errh){
  return 0;
}

//TODO Needs polishing for specific tasks
// Type = 1,2 (for state report), 3,4 for filter mode changes
Packet* MakeIGMPReport::make_packet(IPAddress grpIP, int type){
    int tailroom = 0;
    int packetsize = sizeof(igmp_report_header) + sizeof(igmp_group_record_header);
    int headroom = sizeof(click_ip) + sizeof(click_udp) + sizeof(click_ether);
    WritablePacket* packet = Packet::make(headroom, 0, packetsize, tailroom);
    if (packet == 0) {
		click_chatter("Cannot make packet");
		return 0;
    }
    memset(packet->data(), 0, packet->length());

    igmp_report_header* igmp = (igmp_report_header*) packet->data();
    igmp->type = 0x22; //Membership Report

    igmp->numrecords = htons(1);
    igmp_group_record_header* rec = (igmp_group_record_header*) (packet->data() + sizeof(igmp_report_header));
    rec->type = type;
    rec->auxlen = 0;
    rec->numsrc = htons(0);
    rec->mca = grpIP.addr();
    igmp->checksum = click_in_cksum((unsigned char *)igmp, sizeof(igmp_report_header) + sizeof(igmp_group_record_header));

    return packet;
}

void MakeIGMPReport::push(int, Packet *p){
	output(0).push(p);
}

int MakeIGMPReport::joinHandler(const String &conf, Element *e, void * thunk, ErrorHandler* errh){
    MakeIGMPReport * me = (MakeIGMPReport *) e;
    IPAddress grpIP;
    if (cp_va_kparse(conf, me, errh, "ip", cpkM, cpIPAddress, &grpIP, cpEnd) < 0) {
    	return -1;
    }
    me->execute(grpIP, 4);
    return 0;
}

int MakeIGMPReport::leaveHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh) {
	MakeIGMPReport* element = (MakeIGMPReport*) e;
	IPAddress grpIP;
	if (cp_va_kparse(conf, element, errh, "ip", cpkM, cpIPAddress, &grpIP, cpEnd) < -1) {
		click_chatter("Something went wrong");
	}
	element->execute(grpIP, 3);
	return 0;
}

void MakeIGMPReport::execute(IPAddress grpIP, int type) {
	output(0).push(make_packet(grpIP, type));
}

void MakeIGMPReport::add_handlers(){
    add_write_handler("join_group", &joinHandler, (void *)0);
    add_write_handler("leave_group", &leaveHandler, (void *)0);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(MakeIGMPReport)











