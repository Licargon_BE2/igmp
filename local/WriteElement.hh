#ifndef CLICK_WRITEELEMENT_HH
#define CLICK_WRITEELEMENT_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
CLICK_DECLS

class WriteElement: public Element {
public:
    const char *class_name() const { return "WriteElement";}
    const char *port_count() const { return "0/1"; }
    const char *processing() const { return PUSH; }
    int initialize(ErrorHandler *errh);
    int configure (Vector<String> &, ErrorHandler *);
    static int handle(const String& conf, Element* e, void* thunk, ErrorHandler* errh);
    String giveSomeValue();
    void add_handlers();
    
    
private:
    String group;
    IPAddress ip;
};
CLICK_ENDDECLS
#endif