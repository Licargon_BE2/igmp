#ifndef CLICK_ROUTERINFOBASE_HH
#define CLICK_ROUTERINFOBASE_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include <click/hashtable.hh>

CLICK_DECLS

// We don't have sources so we don't need to keep a useless source record vector
struct state{
    String gAddress ;
    Timer* gTimer ;
    int filterMode;
    int amountOfListeners;
};

class RouterInfoBase: public Element {
public:
    const char *class_name() const { return "RouterInfoBase";}
    // Input 0; From interface 1 (client network 1)
    // Input 1: From interface 2 (client network 2)
    // Input 2: From out IPLookup. Packets destinated for 224.0.0.10
    // Output 0: To interface 1 (CN1)
    // Output 1: To interface 2 (CN2)
    // Output 2: Outgoing UDP packets
    // Output 3: IGMP Report Replies
    const char *port_count() const { return "3/4"; }
    const char *processing() const { return PUSH; }
    int configure (Vector<String> &, ErrorHandler *);    
    void push(int, Packet*);
private:
    struct groupTimerData{
        int network;
        String gAddress;
        RouterInfoBase* me;
    };
   
    Vector<int> _connectedNetworks; // The networks we have discovered that expressed interest
    HashTable<int, Vector<state*>* > _statesPerNetwork;

    static void handleExpiry(Timer*, void*); // Callback
    void expire(int, String);
    state* getGroupState(int network, String groupAddress);
};
CLICK_ENDDECLS
#endif
