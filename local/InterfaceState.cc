#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>
#include "InterfaceState.hh"
#include "IGMPReport.hh"
#include "IGMPGroupRecordHeader.hh"
CLICK_DECLS

InterfaceState::InterfaceState(): _timer(this){}

int InterfaceState::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    default_robustness_value = 2;
    _timer.initialize(this);
    return 0;
}

void InterfaceState::removeGroup(String groupAddr){
	for (Vector<String>::iterator it = groups.begin(); it != groups.end(); ++it){
		if (*it == groupAddr){
			groups.erase(it);
			return;
		}
	}
	return;
}

bool InterfaceState::in_group(String groupAddr){
    for (Vector<String>::iterator it = groups.begin(); it != groups.end(); ++it){
        if (*it == groupAddr) return true;
    }
    return false;
}

// Keep on sending the current packet as long as we have "robustness" left
void InterfaceState::run_timer(Timer* t){
	output(0).push(curPacket);
	--cur_robustness_value;
	if (cur_robustness_value > 0){
		double randomOrig = rand()/(double)RAND_MAX;
		t->schedule_after_msec((int) (randomOrig * 1000));
	} else {
		curPacket = 0;
		cur_robustness_value = default_robustness_value;
	}
}

void InterfaceState::push(int port, Packet* p){
	srand(time(NULL));
	// click_chatter("Random: %f", rand()/(double)RAND_MAX);
    if (port == 0){ // Port 0 only received reports, no testing needed
	    int headroom = sizeof(click_ip) + sizeof(click_ether);
		igmp_report_header* header = (igmp_report_header*) (p->data() + headroom);
		igmp_group_record_header* gheader = (igmp_group_record_header* ) (p->data() + headroom + sizeof(igmp_report_header));
		IPAddress groupAddress = IPAddress(gheader->mca);
		if (gheader->type == 3 && gheader->numsrc == 0){ // leave
			if (in_group(groupAddress.s())){
				click_chatter("Leaving group %s", groupAddress.s().c_str());
				removeGroup(groupAddress.s());
				
				double randomOrig = rand()/(double)RAND_MAX;
				double random = randomOrig*1000;
				cur_robustness_value = default_robustness_value;
				curPacket = p;

				_timer.schedule_after_msec(random); // Start the timer to start sending the packets
			} else{
				click_chatter("Client not currently in group %s", groupAddress.s().c_str());
				p->kill();
			}
		} else if (gheader->type == 4 && gheader->numsrc == 0){ // Join
			if (!in_group(groupAddress.s())){
				click_chatter("Joining group %s", groupAddress.s().c_str());
				groups.push_back(groupAddress.s());

				double randomOrig = rand()/(double)RAND_MAX;
				double random = randomOrig*1000;
				cur_robustness_value = default_robustness_value;
				curPacket = p;

				_timer.schedule_after_msec((int) random); // Start the timer to start sending the packets
			} else{
				click_chatter("Already in a group");
				p->kill();
			}
		} else {
			click_chatter("Something went wrong");
			p->kill();
		}
	} else {
		const click_ip* iph = p->ip_header();
		String dstGroup = IPAddress(iph->ip_dst).s();
		if (in_group(dstGroup)){
			output(1).push(p);
		} else{
		    p->kill();
		}
	}
}


CLICK_ENDDECLS
EXPORT_ELEMENT(InterfaceState)