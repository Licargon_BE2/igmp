#ifndef CLICK_IGMPREPORT_H
#define CLICK_IGMPREPORT_H

// Basic IGMP Report header.There still need to be source addresses added at the end
struct igmp_report_header {
	uint8_t type;
	uint8_t res1;
	uint16_t checksum;
	uint16_t res2;
  	uint16_t numrecords;
};
#endif
