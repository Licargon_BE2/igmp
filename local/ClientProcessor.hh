#ifndef CLICK_CLIENTPROCESSOR_HH
#define CLICK_CLIENTPROCESSOR_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/timer.hh>
#include <click/hashtable.hh>

CLICK_DECLS

// Interface state used in the client.
// Input 0: Reports
// Input 1: UDP Pings
// Input 2; Queries
// Output 0: Outgoing reports from handlercall
// Output 1: Accepted pings
// Output 2: Rejected pings
// Output 3: Outgoing reports forwarded from InterfaceState
// Output 4: Outgoing query responses

class ClientProcessor: public Element {
public:
	ClientProcessor();
    const char *class_name() const { return "ClientProcessor";}
    const char *port_count() const { return "3/5"; }
    const char *processing() const { return PUSH; } 
    int configure (Vector<String> &, ErrorHandler *);    
    void push(int, Packet*);
    void run_timer(Timer*);
private:
	// InterfaceState code
	bool in_group(String);
	void removeGroup(String);
	Vector<String> groups;
	Timer* _timer;
	int default_robustness_value;
	int cur_robustness_value;
	Packet* curPacket;

	// ProcessQuery code
	bool responsePending;
    Timer* generalQueryTimer;    
    bool responseToGeneralQuery;
    Packet* currentGeneralResponse; // Current report as a response
    Vector<Packet*> currentGeneralResponses;
    Vector<Timer*> groupSpecificTimers;
    Vector<Packet*> groupSpecificResponses;
    Vector<String> groupResponseDestinations;
    bool group_timer_scheduled(String group);
    Timer* replace_response(String group, Packet* response);

    // MakeIGMPReport code
    Packet* make_packet(String, int);
    static int joinHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh);
	static int leaveHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh);
	void add_handlers();
	void execute(IPAddress grpIP, int type);

    void removeGroupQueryReply(String group, Timer* timer, Packet* packet);
};
CLICK_ENDDECLS
#endif