#ifndef CLICK_MAKEIGMPREPORT_HH
#define CLICK_MAKEIGMPREPORT_HH
#include <click/element.hh>
#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include "IGMPReport.hh"
#include <click/timer.hh>
#include <click/element.hh>
CLICK_DECLS

class MakeIGMPReport: public Element {
public:
	MakeIGMPReport();
	~MakeIGMPReport();
	const char *class_name() const { return "MakeIGMPReport";}
	const char *port_count() const { return "0/1"; }
	const char *processing() const { return PUSH; }
	static int joinHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh);
	static int leaveHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh);
	Packet* make_packet(IPAddress grpIP, int type);
	int initialize(ErrorHandler *errh);
	int configure (Vector<String> &, ErrorHandler *);
	void push(int, Packet*);
    void add_handlers();
	void execute(IPAddress grpIP, int type);

};
CLICK_ENDDECLS
#endif
