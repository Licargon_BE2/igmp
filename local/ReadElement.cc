#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include "ReadElement.hh"
CLICK_DECLS

int ReadElement::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    return 0;
}

int ReadElement::initialize(ErrorHandler* errh){
    return 0;
}

String ReadElement::giveSomeValue(){

}

/*String ReadElement::handle(Element *e, void * thunk){
    
    IGMPJoin * test = new IGMPJoin();
    test.make_packet();
    ReadElement * me = (ReadElement *) e;
    click_chatter(me->giveSomeValue().c_str());
    return me->giveSomeValue();
}

void ReadElement::add_handlers(){
    add_read_handler("testhandle", &handle, (void *)0);
}*/

CLICK_ENDDECLS
EXPORT_ELEMENT(ReadElement)
