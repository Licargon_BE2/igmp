#ifndef CLICK_GENERATEIGMPMEMBERSHIPQUERY_HH
#define CLICK_GENERATEIGMPMEMBERSHIPQUERY_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include "IGMPMembershipQueryHeader.hh"
#include "IGMPJoin.hh"
#include "IGMPReport.hh"
#include "IGMPGroupRecordHeader.hh"
CLICK_DECLS

class GenerateIGMPMembershipQuery: public Element {
public:
	GenerateIGMPMembershipQuery();
    const char *class_name() const { return "GenerateIGMPMembershipQuery";}
    const char *port_count() const { return "1/1"; }
    const char *processing() const { return PUSH; }
    int initialize(ErrorHandler *errh);
    int configure (Vector<String> &, ErrorHandler *);
    void run_timer(Timer*); 
	void push(int, Packet*);
    Packet* make_packet(String, bool); 
private:
	Timer* generalTimer;
	Timer* delayTimer;
	bool _blocked;
};
CLICK_ENDDECLS
#endif
