#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include "ProcessQuery.hh"
#include "IGMPMembershipQueryHeader.hh"
CLICK_DECLS


void ProcessQuery::run_timer(Timer* t){
    if (responseToGeneralQuery && responsePending){
		click_chatter("General query thingy expired");
		output(0).push(currentGeneralResponse);
		
    } else {
		for (int i = 0; i < groupSpecificTimers.size(); ++i){
		    if (groupSpecificTimers[i] == t){
			click_chatter("Group timer expired");
			// Send out response to group
		    }
		}
    }
	
    return;
}

void ProcessQuery::removeGroupTimer(Timer* t){
    for (Vector<Timer*>::iterator it = groupSpecificTimers.begin(); it != groupSpecificTimers.end(); ++it)
		if ((*it) == t) groupSpecificTimers.erase(it);
}

int ProcessQuery::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    responsePending = false;
    groupSpecificTimers = Vector<Timer*>();
    groupSpecificResponses = Vector<Packet*>();
    groupResponseDestinations = Vector<String>();
    responseToGeneralQuery = false;
    generalQueryTimer = new Timer(this);
    return 0;
}

// Returns true if there is a response scheduled for this group
bool ProcessQuery::group_timer_scheduled(String group){
    for (Vector<String>::iterator it = groupResponseDestinations.begin(); it != groupResponseDestinations.end(); ++it){
		if ((*it) == group) return true;
    }
    return false;
}

Timer* ProcessQuery::replace_response(String group, Packet* response){
    for (int i = 0; i < groupResponseDestinations.size(); ++i){
		if (groupResponseDestinations[i] == group){
		    groupSpecificResponses[i] = response;
		    return groupSpecificTimers[i];
		}
    }
}


void ProcessQuery::push(int port, Packet* p){		
    int headroom = sizeof(click_ip) + sizeof(click_ether);
    igmp_membership_query_header* header = (igmp_membership_query_header*) (p->data() + headroom);
    if (header->type == 0x11){ // Query type
	srand(time(NULL));
	// Calculate response time according to the max resp. code in the query
	double responseTime = 1000.0 * rand() / (double) RAND_MAX * (double) header->respCode/10.0;
	Timestamp now = Timestamp::now_steady();
	Timestamp curTimerExpiry = generalQueryTimer->expiry_steady();
	
	// RFC page 22 cases
	if (responsePending && responseToGeneralQuery){ // Case 1: Already response to general query
	    if ( (now + Timestamp(0, responseTime) > curTimerExpiry) ){ // Current response sooner than calculated delay: Do nothing
		return;
	    } 
	} else { // Case 2: Not a pending response, or no response pending to general query AND received = general: replace
	    if (header->groupAddress == 0){ // Case 2: General query received
		responsePending = true;
		responseToGeneralQuery = true;
		generalQueryTimer->unschedule();
		if (!generalQueryTimer->initialized()) generalQueryTimer->initialize(this);
		currentGeneralResponse = p; // TODO: Fill in with right report
		generalQueryTimer->schedule_after_msec((int) responseTime);
		
	    } else if (!responsePending) { // Case 3: Group specific query, and none scheduled yet for that group
		String group = IPAddress(header->groupAddress).s();
		if (!group_timer_scheduled(group)){ // Not a response scheduled yet for this group
		    Timer* newTimer = new Timer(this);
		    newTimer->initialize(this);
		    Packet* response = 0;
		    groupSpecificResponses.push_back(response);
		    groupSpecificTimers.push_back(newTimer);		
		    newTimer->schedule_after_msec(responseTime);
		} else { // Case 4: replace the current response with a new one
		    Packet* newResponse = 0;
		    Timer* groupTimer = replace_response(group, newResponse);
		    if (groupTimer != 0){
			groupTimer->unschedule();
			Timestamp now = Timestamp::now_steady();
			Timestamp curTimerExpiry = groupTimer->expiry_steady();
			Timestamp diff = curTimerExpiry - now;
			int timeLeft = (int) diff.doubleval();
			int newTime = ((int)responseTime < timeLeft) ? responseTime : timeLeft;
			groupTimer->schedule_after_msec(newTime);
		    }
		    
		}
	    }	
	}
	//click_chatter("Max resp time: %f seconds", (double)header->respCode/10.0);
    }
	
	double randomInt = rand() / (double) RAND_MAX * (double) header->respCode/10.0;
	//click_chatter("Random time to wait: %f", randomInt);

}


CLICK_ENDDECLS
EXPORT_ELEMENT(ProcessQuery)

