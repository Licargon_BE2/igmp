#ifndef CLICK_READELEMENT_HH
#define CLICK_READELEMENT_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
CLICK_DECLS

class ReadElement: public Element {
public:
    const char *class_name() const { return "ReadElement";}
    const char *port_count() const { return "0/1"; }
    const char *processing() const { return PUSH; }
    int initialize(ErrorHandler *errh);
    int configure (Vector<String> &, ErrorHandler *);
    //static String handle(Element *e, void * thunk);
    String giveSomeValue();
    //void add_handlers();
};
CLICK_ENDDECLS
#endif
