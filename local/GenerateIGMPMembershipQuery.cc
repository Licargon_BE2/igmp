#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include "GenerateIGMPMembershipQuery.hh"
#include "IGMPJoin.hh"
CLICK_DECLS

// Contructor. Init the timer.
GenerateIGMPMembershipQuery::GenerateIGMPMembershipQuery(): _blocked(false){}

int GenerateIGMPMembershipQuery::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    generalTimer = new Timer(this);
    generalTimer->initialize(this);
    delayTimer = new Timer(this);
    delayTimer->initialize(this);
    //generalTimer->schedule_now();
    generalTimer->schedule_after_msec(5000);
    return 0;
}

// Right now: send a packet ever 1 second to test things out
int GenerateIGMPMembershipQuery::initialize(ErrorHandler* errh){
    return 0;
}

void GenerateIGMPMembershipQuery::push(int port, Packet* p){
	int headroom = sizeof(click_ip) + sizeof(click_ether);
	igmp_report_header* header = (igmp_report_header*) (p->data() + headroom);
	igmp_group_record_header* gheader = (igmp_group_record_header* ) (p->data() + headroom + sizeof(igmp_report_header));
	if ( header->type == 0x22 && (gheader->type == 1 || gheader->type == 2)) {
		click_chatter("Received a report reply");

		delayTimer->schedule_now();
		return;	
	} else if(header->type == 0x22 && gheader->type == 3){ // Client left a group -> schedule group specific query
		click_chatter("QueryGenerator -- Client left group %s, sending group specific query", IPAddress(gheader->mca).s().c_str());
		output(0).push(make_packet(IPAddress(gheader->mca).s().c_str(), false));
	}
	return;
}


void GenerateIGMPMembershipQuery::run_timer(Timer* timer){
	if  ( timer == this->generalTimer ) {
		if (!_blocked) {
			click_chatter("GenerateIGMPMembershipQuery -- Sending query");
			output(0).push(make_packet("", true));
			timer->schedule_after_msec(125000); // Standard interval: Send a general query every 125 seconds
		}
		timer->schedule_after_msec(125000);
	}
	else if ( timer == this->delayTimer ) {
		if ( _blocked ) {
			_blocked = false;
		}
	}
	else {
		_blocked = true;
		timer->schedule_after_sec(2*125*10);
	}
}  

Packet* GenerateIGMPMembershipQuery::make_packet(String grpIP, bool generalQuery){
    int tailroom = 0;
    int packetsize = sizeof(igmp_membership_query_header);
    int headroom = sizeof(click_ip) + sizeof(click_udp) + sizeof(click_ether);
    WritablePacket* packet = Packet::make(headroom, 0, packetsize, tailroom);
    if (packet == 0) { 
      click_chatter("Cannot make packet");
      return 0;
    }
    memset(packet->data(), 0, packet->length());
    
    igmp_membership_query_header* igmp = (igmp_membership_query_header*) packet->data();
    igmp->type = 0x11;
    igmp->respCode = 100; // Calculated in tenths of a second. 100 = 10.0s = default value
    if (generalQuery)
	   igmp->groupAddress = 0;
    else
	   igmp->groupAddress = IPAddress(grpIP).addr();
    igmp->resANDsqrv = 2;
    igmp->QQIC = 10;
    igmp->checksum = click_in_cksum((unsigned char *)igmp, sizeof(igmp_membership_query_header));
       
    return packet;    
}



CLICK_ENDDECLS
EXPORT_ELEMENT(GenerateIGMPMembershipQuery)
