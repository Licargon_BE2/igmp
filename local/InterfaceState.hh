#ifndef CLICK_INTERFACESTATE_HH
#define CLICK_INTERFACESTATE_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/timer.hh>
CLICK_DECLS

// Interface state used in the client.
// Input 0: Reports
// Input 1: UDP Pings
// Output 0: Forwarded reports
// Output 1: Accepted pings
// Output 2: Rejected pings
class InterfaceState: public Element {
public:
	InterfaceState();
    const char *class_name() const { return "InterfaceState";}
    const char *port_count() const { return "2/3"; }
    const char *processing() const { return PUSH; } 
    int configure (Vector<String> &, ErrorHandler *);    
    void push(int, Packet*);
    void run_timer(Timer*);
private:
	bool in_group(String);
	void removeGroup(String);
	Vector<String> groups;
	Timer _timer;
	int default_robustness_value;
	int cur_robustness_value;
	Packet* curPacket;
};
CLICK_ENDDECLS
#endif