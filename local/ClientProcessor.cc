#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>
#include <clicknet/udp.h>
#include "ClientProcessor.hh"
#include "IGMPReport.hh"
#include "IGMPGroupRecordHeader.hh"
#include "IGMPMembershipQueryHeader.hh"

CLICK_DECLS

ClientProcessor::ClientProcessor(){}

int ClientProcessor::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    default_robustness_value = 2;
    _timer = new Timer(this);
    _timer->initialize(this);
    currentGeneralResponses = Vector<Packet*>();

    // ProcessQuery code
    responsePending = false;
    groupSpecificTimers = Vector<Timer*>();
    groupSpecificResponses = Vector<Packet*>();
    groupResponseDestinations = Vector<String>();
    responseToGeneralQuery = false;
    generalQueryTimer = new Timer(this);
    return 0;
}

void ClientProcessor::removeGroup(String groupAddr){
	for (Vector<String>::iterator it = groups.begin(); it != groups.end(); ++it){
		if (*it == groupAddr){
			groups.erase(it);
			return;
		}
	}
	return;
}

bool ClientProcessor::in_group(String groupAddr){
    for (Vector<String>::iterator it = groups.begin(); it != groups.end(); ++it){
        if (*it == groupAddr) return true;
    }
    return false;
}

// This bit gives segfaults...
void ClientProcessor::removeGroupQueryReply(String group, Timer* timer, Packet* packet){
	for (Vector<Packet*>::iterator it = groupSpecificResponses.begin(); it != groupSpecificResponses.end(); ++it){
		if (*it == packet){
			groupSpecificResponses.erase(it);
		}
	}
	for (Vector<String>::iterator it = groupResponseDestinations.begin(); it != groupResponseDestinations.end(); ++it){
		if (*it == group){
			groupResponseDestinations.erase(it);
		}
	}
	for (Vector<Timer*>::iterator it = groupSpecificTimers.begin(); it != groupSpecificTimers.end(); ++it){
		if (*it == timer){
			groupSpecificTimers.erase(it);
		}
	}
}

void ClientProcessor::run_timer(Timer* t){
	if (t == _timer){
		igmp_report_header* header = (igmp_report_header*) (curPacket->data() + sizeof(click_ip) + sizeof(click_ether));
		header->type = 0x22; // Sometimes the type tends to reset
		output(3).push(curPacket);
		--cur_robustness_value;
		if (cur_robustness_value > 0){
			double randomOrig = rand()/(double)RAND_MAX;
			t->schedule_after_msec((int) (randomOrig * 1000));
		} else {
			curPacket = 0;
			cur_robustness_value = default_robustness_value;
		}
	} else{
		if (responseToGeneralQuery && responsePending){
			for (Vector<Packet*>::iterator it = currentGeneralResponses.begin(); it != currentGeneralResponses.end(); ++it){
				output(4).push(*it);
			}
			responsePending = false;
			responseToGeneralQuery = false;
	    } else {
			for (int i = 0; i < groupSpecificTimers.size(); ++i){
			    if (groupSpecificTimers[i] == t){
					click_chatter("ClientProcessor -- Group timer for group %d ran out. Sending reply.", groupResponseDestinations[i].c_str());
					output(4).push(groupSpecificResponses[i]);
					//removeGroupQueryReply(groupResponseDestinations[i], groupSpecificTimers[i], groupSpecificResponses[i]);
					
			    }
			}
	    }
	}
}

// Type = 1,2 (for state report), 3,4 for filter mode changes
// Created IGMP report packet for a certain group with a certain type
Packet* ClientProcessor::make_packet(String grpIP, int type){
    int tailroom = 0;
    int packetsize = sizeof(igmp_report_header) + sizeof(igmp_group_record_header);
    int headroom = sizeof(click_ip) + sizeof(click_udp) + sizeof(click_ether);
    WritablePacket* packet = Packet::make(headroom, 0, packetsize, tailroom);
    if (packet == 0) {
		click_chatter("lientProcessor -- Cannot make packet");
		return 0;
    }
    memset(packet->data(), 0, packet->length());

    igmp_report_header* igmp = (igmp_report_header*) packet->data();
    igmp->type = 0x22; //Membership Report

    igmp->numrecords = htons(1);
    igmp_group_record_header* rec = (igmp_group_record_header*) (packet->data() + sizeof(igmp_report_header));
    rec->type = type;
    rec->auxlen = 0;
    rec->numsrc = htons(0);
    if (grpIP == "") rec->mca = 0;
    else rec->mca = IPAddress(grpIP).addr();
    igmp->checksum = click_in_cksum((unsigned char *)igmp, sizeof(igmp_report_header) + sizeof(igmp_group_record_header));

    return packet;
}

int ClientProcessor::joinHandler(const String &conf, Element *e, void * thunk, ErrorHandler* errh){
    ClientProcessor * me = (ClientProcessor *) e;
    IPAddress grpIP;
    if (cp_va_kparse(conf, me, errh, "ip", cpkM, cpIPAddress, &grpIP, cpEnd) < 0) {
    	return -1;
    }
    me->execute(grpIP, 4);
    return 0;
}

int ClientProcessor::leaveHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh) {
	ClientProcessor* element = (ClientProcessor*) e;
	IPAddress grpIP;
	if (cp_va_kparse(conf, element, errh, "ip", cpkM, cpIPAddress, &grpIP, cpEnd) < -1) {
		click_chatter("ClientProcessor -- Something went wrong");
	}
	element->execute(grpIP, 3);
	return 0;
}

void ClientProcessor::execute(IPAddress grpIP, int type) {
	output(0).push(make_packet(grpIP.s(), type));
}

void ClientProcessor::add_handlers(){
    add_write_handler("join_group", &joinHandler, (void *)0);
    add_write_handler("leave_group", &leaveHandler, (void *)0);
}

void ClientProcessor::push(int port, Packet* p){
	srand(time(NULL));
	// click_chatter("Random: %f", rand()/(double)RAND_MAX);
    if (port == 0){ // Port 0 only received reports, no testing needed
	    int headroom = sizeof(click_ip) + sizeof(click_ether);
		igmp_report_header* header = (igmp_report_header*) (p->data() + headroom);
		igmp_group_record_header* gheader = (igmp_group_record_header* ) (p->data() + headroom + sizeof(igmp_report_header));
		IPAddress groupAddress = IPAddress(gheader->mca);
		if (gheader->type == 3 && gheader->numsrc == 0){ // leave
			if (in_group(groupAddress.s())){
				click_chatter("ClientProcessor -- Leaving group %s", groupAddress.s().c_str());
				removeGroup(groupAddress.s());
				
				double randomOrig = rand()/(double)RAND_MAX;
				double random = randomOrig*1000;
				cur_robustness_value = default_robustness_value;
				curPacket = p;

				_timer->schedule_after_msec((int) random); // Start the timer to start sending the packets
			} else{
				click_chatter("ClientProcessor -- Client not currently in group %s", groupAddress.s().c_str());
				p->kill();
			}
		} else if (gheader->type == 4 && gheader->numsrc == 0){ // Join
			if (!in_group(groupAddress.s())){
				click_chatter("ClientProcessor -- Joining group %s", groupAddress.s().c_str());
				groups.push_back(groupAddress.s());

				double randomOrig = rand()/(double)RAND_MAX;
				double random = randomOrig*1000;
				cur_robustness_value = default_robustness_value;
				curPacket = p;

				_timer->schedule_after_msec((int) random); // Start the timer to start sending the packets
			} else{
				click_chatter("ClientProcessor -- Client already in group %s", groupAddress.s().c_str());
				p->kill();
			}
		} else {
			click_chatter("ClientProcessor -- Something went wrong");
			p->kill();
		}
	} else if (port == 1) { // Incoming ping: See if we joined this group, if so: accept it and locally deliver
		const click_ip* iph = p->ip_header();
		String dstGroup = IPAddress(iph->ip_dst).s();
		if (in_group(dstGroup)){
			output(1).push(p);
		} else{
		    p->kill();
		}
	} else { // Incoming queries
		int headroom = sizeof(click_ip) + sizeof(click_ether);
	    igmp_membership_query_header* header = (igmp_membership_query_header*) (p->data() + headroom);
	    if (header->type == 0x11){ // Query type
			srand(time(NULL));
			// Calculate response time according to the max resp. code in the query
			double responseTime = 1000.0 * rand() / (double) RAND_MAX * (double) header->respCode/10.0;
			Timestamp now = Timestamp::now_steady();
			Timestamp curTimerExpiry = generalQueryTimer->expiry_steady();
			
			// RFC page 22 cases
			if (responsePending && responseToGeneralQuery){ // Case 1: Already response to general query
			    if ( (now + Timestamp(0, responseTime) > curTimerExpiry) ){ // Current response sooner than calculated delay: Do nothing
				return;
			    } 
			} else { // Case 2: Not a pending response, or no response pending to general query AND received = general: replace
			    if (header->groupAddress == 0){ // Case 2: General query received
					responsePending = true;
					responseToGeneralQuery = true;
					generalQueryTimer->unschedule();
					if (!generalQueryTimer->initialized()) generalQueryTimer->initialize(this);
					for (Vector<String>::iterator it = groups.begin(); it != groups.end(); ++it){ // make a packet for each group we're in
						currentGeneralResponses.push_back(make_packet(*it, 2));
					}			
					
					generalQueryTimer->schedule_after_msec((int) responseTime);
			    } else if (!responsePending) { // Case 3: Group specific query, and none scheduled yet for that group
					String group = IPAddress(header->groupAddress).s();
					if (!group_timer_scheduled(group)){ // Not a response scheduled yet for this group
						click_chatter("ClientProcessor -- Not a response scheduled yet for this group");
					    Timer* newTimer = new Timer(this);
					    newTimer->initialize(this);
					    Packet* response = make_packet(group, 2);
					    groupSpecificResponses.push_back(response);
					    groupSpecificTimers.push_back(newTimer);	
					    groupResponseDestinations.push_back(group);	

					    newTimer->schedule_after_msec(responseTime);
					} else { // Case 4: replace the current response with a new one
						click_chatter("ClientProcessor -- Already a response pending for this group.");
					    Packet* newResponse = make_packet(group, 2);
					    Timer* groupTimer = replace_response(group, newResponse);
					    if (groupTimer != 0){
						groupTimer->unschedule();
						Timestamp now = Timestamp::now_steady();
						Timestamp curTimerExpiry = groupTimer->expiry_steady();
						Timestamp diff = curTimerExpiry - now;
						int timeLeft = (int) diff.doubleval();
						int newTime = ((int)responseTime < timeLeft) ? responseTime : timeLeft;
						groupTimer->schedule_after_msec(newTime);
					}
				    
				}
	  	 	}	
		}
	//click_chatter("Max resp time: %f seconds", (double)header->respCode/10.0);
    }	
	}
}

// Returns true if there is a response scheduled for this group
bool ClientProcessor::group_timer_scheduled(String group){
    for (Vector<String>::iterator it = groupResponseDestinations.begin(); it != groupResponseDestinations.end(); ++it){
		if ((*it) == group) return true;
    }
    return false;
}

Timer* ClientProcessor::replace_response(String group, Packet* response){
    for (int i = 0; i < groupResponseDestinations.size(); ++i){
		if (groupResponseDestinations[i] == group){
		    groupSpecificResponses[i] = response;
		    return groupSpecificTimers[i];
		}
    }
}



CLICK_ENDDECLS
EXPORT_ELEMENT(ClientProcessor)