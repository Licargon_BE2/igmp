#ifndef CLICK_IGMPMEMBERSHIPREPORTHEADER_H
#define CLICK_IGMPMEMBERSHIPREPORTHEADER_H

// Basic IGMP Report header.There still need to be source addresses added at the end
struct igmp_membership_report_header {
	uint8_t type;
	uint16_t checksum;
  	uint16_t numrecords;
};
#endif
