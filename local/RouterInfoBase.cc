#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/timer.hh>
#include "RouterInfoBase.hh"
#include "IGMPReport.hh"
#include "IGMPGroupRecordHeader.hh"
CLICK_DECLS

int RouterInfoBase::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;

    _connectedNetworks = Vector<int>();
    _statesPerNetwork = HashTable<int, Vector<state*>* >();
    return 0;
}

bool find(Vector<String> clientList, String clientAddr){
    for (Vector<String>::iterator it = clientList.begin(); it != clientList.end(); ++it){
        if (*it == clientAddr) return true;
    }
    return false;
}

bool find(Vector<int> list, int interface){
	for (Vector<int>::iterator it = list.begin(); it != list.end(); ++it){
        if (*it == interface) return true;
    }
    return false;
} 

void RouterInfoBase::handleExpiry(Timer* t, void* data){
	groupTimerData* timerData = (groupTimerData*) data;
	assert(timerData);
	timerData->me->expire(timerData->network, timerData->gAddress);

}

void RouterInfoBase::expire(int network, String group){
	Vector<state*>* states = _statesPerNetwork.get(network);
	for (Vector<state*>::iterator it = states->begin(); it!= states->end(); it++){
		if ((*it)->gAddress == group){
			states->erase(it);
			click_chatter("RouterInfoBase -- Grouptimer for group %s expired. Group record has been deleted.", group.c_str());
			break;

		}
	}
}

state* RouterInfoBase::getGroupState(int network, String groupAddress){
	if (!find(_connectedNetworks, network)) return 0;
	Vector<state*>* states = _statesPerNetwork.get(network);
	for (Vector<state*>::iterator it = states->begin(); it!= states->end(); it++){
		if ((*it)->gAddress == groupAddress){
			return *it;
		}
	}
	return 0;
}


void RouterInfoBase::push(int port, Packet* p){
	int INCLUDE = 1;
	int EXCLUDE = 2;
	int TO_INCLUDE = 3;
	int TO_EXCLUDE = 4;

	int headroom = sizeof(click_ip) + sizeof(click_ether);
	igmp_report_header* header = (igmp_report_header*) (p->data() + headroom);
	igmp_group_record_header* gheader = (igmp_group_record_header* ) (p->data() + headroom + sizeof(igmp_report_header));
	IPAddress groupAddress = IPAddress(gheader->mca);
	const click_ip* ip_h = p->ip_header();
	IPAddress src = IPAddress(ip_h->ip_src);
    if (port < 2) { // Port 0: Incoming from CN1, Port 1: Incoming from CN2
    	if (gheader->type == INCLUDE || gheader->type == EXCLUDE){ // reply report received
    		state* state = getGroupState(port, groupAddress.s());
    		// RFC p31 table cases
    		if (gheader->type == EXCLUDE && state != 0 && state->filterMode == INCLUDE){ // Case 2
    			state->gTimer->unschedule();
    			state->gTimer->schedule_after_sec(2*10*125); // Default GMI value
    			click_chatter("RouterInfoBase -- Group timer rescheduled.");
    		} else if (gheader->type == EXCLUDE && state != 0 && state->filterMode == EXCLUDE){ // Case 4
    			state->gTimer->unschedule();
    			state->gTimer->schedule_after_sec(2*10*125); // Default GMI value
    			click_chatter("RouterInfoBase -- Group timer rescheduled.");
    		}
       		output(3).push(p); // Push it so it can get to the query element
    		return;
    	} else if ( (gheader->type == TO_INCLUDE || gheader-> type == TO_EXCLUDE) && gheader->numsrc == 0){ // Filter mode change report received
    		Vector<state*>* states; // States for this network
			if (!find(_connectedNetworks, port)){
				_connectedNetworks.push_back(port);
				states = new Vector<state*>();
				_statesPerNetwork.set(port, states);
			} else {
				states = _statesPerNetwork.get(port);
			}
			state* groupState; // Temp variable to hold a possible state of the group we are interested in
			bool found = false;
			for (int i = 0; i < states->size(); ++i){
				if ((*states)[i]->gAddress == groupAddress.s()){
					groupState = (*states)[i];
					found = true;
				}
			}
		
			if (!found){
				groupState = new state;
				groupState->gAddress = groupAddress.s();
				groupState->amountOfListeners = 1;

				groupTimerData* timerData = new groupTimerData();
				timerData->gAddress = groupAddress.s();
				timerData->network = port;
				timerData->me = this;
				groupState->gTimer = new Timer(&RouterInfoBase::handleExpiry, timerData);
				groupState->gTimer->initialize(this);
				//groupState->gTimer->schedule_after_msec(2500);

				if (gheader->type == 3){
					groupState->filterMode = INCLUDE;
				} else {
					groupState->filterMode = EXCLUDE;
					groupState->gTimer->schedule_after_sec(2*125*10);

				}
				states->push_back(groupState);
			} else { // Existing state found
				if (gheader->type == 3){ // Perform leave
					groupState->amountOfListeners--;
					if (groupState->filterMode == EXCLUDE && groupState->amountOfListeners/2 <= 0){
						output(3).push(p); // Forward it the querier so we can send out a group specific query
					} 
					if (groupState->amountOfListeners/2 <= 0){
						groupState->filterMode = INCLUDE;
					}
				} else { // Perform join
					if (groupState->filterMode == INCLUDE){
						groupState->filterMode = EXCLUDE;
						groupState->gTimer->unschedule(); // Reschedule the group timer
    					groupState->gTimer->schedule_after_sec(2*125*10); // Default GMI value, RFC p 32 
					} else {
						groupState->amountOfListeners++;
						click_chatter("RouterInfoBase -- Client on network %d joined group %d, group timer has been rescheduled.", port, groupAddress.s().c_str());
					}
				}
				
			}
			_statesPerNetwork.set(port, states);
    	}
		
	} else { // Port with incoming udp multicast packets
		// Figure out the group where our packet is sent to
		const click_ip* ip_h = p->ip_header();
		IPAddress groupAddress = IPAddress(ip_h->ip_dst);
		for (Vector<int>::iterator network = _connectedNetworks.begin(); network != _connectedNetworks.end(); ++network){ // Loop over all discovered networks
			Vector<state*>* states = _statesPerNetwork.get(*network);
			for (Vector<state*>::iterator it =states->begin(); it != states->end(); ++it){
				state* temp = *it;
				if (temp->gAddress == groupAddress.s() && temp->filterMode == EXCLUDE){ // We found an interested system on this network for this group: send a packet out
					Packet* toSend = p->clone();
					output(*network).push(toSend);
					break;
				}
			}
		}
	}

}


CLICK_ENDDECLS
EXPORT_ELEMENT(RouterInfoBase)
