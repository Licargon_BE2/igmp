#ifndef CLICK_IGMPMEMBERSHIPQUERYHEADER_H
#define CLICK_IGMPMEMBERSHIPQUERYHEADER_H

// Basic IGMP header.There still need to be source addresses added at the end
struct igmp_membership_query_header {
  uint8_t type;
  uint8_t respCode;
  uint16_t checksum;
  uint32_t groupAddress;
  uint8_t resANDsqrv;
  uint8_t QQIC;
};
#endif
