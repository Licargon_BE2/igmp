#ifndef CLICK_IGMPGROUPRECORDHEADER_H
#define CLICK_IGMPGROUPRECORDHEADER_H

// Basic IGMP Group Record header.There still need to be source addresses added at the end
struct igmp_group_record_header {
	uint8_t type;
	uint8_t auxlen;
	uint16_t numsrc;
	uint32_t mca;
	//LIST of source addressees

};
#endif
