#ifndef CLICK_PROCESSQUERY_HH
#define CLICK_PROCESSQUERY_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include <click/timer.hh>

#include <click/hashtable.hh>

CLICK_DECLS

class ProcessQuery: public Element {
public:
    const char *class_name() const { return "ProcessQuery";}
    // Input 0; Incoming queries
    // Output 0: Outgoing reports
    const char *port_count() const { return "1/1"; }
    const char *processing() const { return PUSH; }
    int configure (Vector<String> &, ErrorHandler *);    
    void push(int, Packet*);
    void run_timer(Timer*);
    void execute(Packet*, bool);
    void removeGroupTimer(Timer*);
private:
    bool responsePending;
    
    Timer* generalQueryTimer;    
    bool responseToGeneralQuery;
    Packet* currentGeneralResponse; // Current report as a response
    
    Vector<Timer*> groupSpecificTimers;
    Vector<Packet*> groupSpecificResponses;
    Vector<String> groupResponseDestinations;
    
    bool group_timer_scheduled(String group);
    Timer* replace_response(String group, Packet* response);
    
};
CLICK_ENDDECLS
#endif
