#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include "WriteElement.hh"
CLICK_DECLS

int WriteElement::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    return 0;
}

int WriteElement::initialize(ErrorHandler* errh){
    return 0;
}

String WriteElement::giveSomeValue(){
    return "Test Write";
}

int WriteElement::handle(const String &conf, Element *e, void * thunk, ErrorHandler* errh){
    WriteElement * me = (WriteElement *) e;
    String ip;
    if (cp_va_kparse(conf, me, errh, "ip", cpkM, cpString, &ip, cpEnd) < 0) return -1;
    click_chatter(ip.c_str());
    return 0;
}

void WriteElement::add_handlers(){
    add_write_handler("join_group", &handle, (void *)0);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(WriteElement)