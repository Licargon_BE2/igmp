#ifndef CLICK_IGMPJOIN_HH
#define CLICK_IGMPJOIN_HH
#include <click/element.hh>
#include <click/timer.hh>
#include "IGMPMembershipQueryHeader.hh"

CLICK_DECLS

class IGMPJoin : public Element{
public:
	IGMPJoin();
	~IGMPJoin();
	const char *class_name() const { return "IGMPJoin";}
	const char *port_count() const { return "0/1"; }
	const char *processing() const { return PUSH; }
	static int joinHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh);
	static int leaveHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh);
	int initialize(ErrorHandler *errh);
	void run_timer(Timer *);
	int configure (Vector<String> &, ErrorHandler *);
	void push(int, Packet*);
    void add_handlers();
	void execute(String grpIP);
private:
    Timer _timer;
    Packet* make_packet(String grpIP);
};

CLICK_ENDDECLS
#endif
